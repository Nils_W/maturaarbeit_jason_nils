# Einige Funktionen, welche die Spielsituation und die daraus folgenden Möglichkeiten zurückgeben.
# Wird vom Spielablauf sowie sämtlichen Algorithmen verwendet. → Funktionen zum Teilen.
# Manche senden nur True / False zurück, andere auch listen.
# Überall wird die Liste mit den Daten überreicht, nur Lesefunktion.


def ist_feld_frei(liste, feld, wert = 0):
    # Sollte eigentlich selbsterklärend sein.
    # Die Variable Wert kann benutzt werden, um spezifisch nach etwas anderem zu suchen.
    if liste[feld[0]][feld[1]] == wert:
        return True
    else:
        return False


def fahren_k(liste, startfeld, zielfeld):
    # Kann von einem Feld (Startfeld) auf ein anderes (Zielfeld) gefahren werden?

    if not ist_feld_frei(liste, zielfeld):
        # Falls das gewünschte Ziel besetzt ist, ist der Zug nicht möglich
        return False

    elif zielfeld[0] == startfeld[0] and zielfeld[1] - startfeld[1] in [1, -1]:
        print("gleicher Ring S0")
        # Liegen die Felder auf demselben Ring Nebeneinander?
        return True

    elif zielfeld[0] == startfeld[0] and zielfeld[1] == 0 and startfeld[1] == 7:
        # Sonderfall 1: Ecke oben links, über das Ende des Ringes hinweg zurück zum anfang
        print("gleicher Ring S1")
        return True

    elif zielfeld[0] == startfeld[0] and zielfeld[1] == 7 and startfeld[1] == 0:
        # Sonderfall 2: Ebenfalls über das Ende der Nummerierung, jedoch andere Richtung
        print("gleicher Ring S2")
        return True


    elif startfeld[1] == zielfeld[1] and startfeld[1] % 2 == 1 and startfeld[0] - zielfeld[0] in [-1, 1]:
        print("senkrecht S10")
        # Von aussen nach innen respektive umgekehrt
        # 1. Beide auf derselben Position
        # 2. Position muss eine Verschiebung von aussen/innen zulassen
        # 3. Müssen auf einem unterschiedlichen Ring sein, direkt angrenzend.
        return True

    else:
        return False


def anz_steine(liste):
    # Gibt die Anzahl der Steine der beiden Spieler zurück. [p1, p2]
    anzahl_steine = [0, 0]
    for i in range(2):
        for ring in liste:
            anzahl_steine[i] += ring.count(i+1)
    return anzahl_steine


def muehle_ueberpruefung(liste, alte_muehlen = False):
    # Das Spiel wird nach mühlen abgesucht.
    # Falls keine alten Mühlen existieren werden unmögliche Werte in diese Liste gesetzt,
    # für den Vergleich am schluss notwendig.
    if not alte_muehlen:
        alte_muehlen = [[5, 5], [7, 7]]
    # Neue gibt es aber zu Beginn definitiv keine.
    neue_muehlen = []
    # überprüfung af sämtliche, ringförmige Mühlen.
    # Diese können sich an den folgenden Stellen bilden.
    ring_muehlen = [[0, 1, 2], [2, 3, 4], [4, 5, 6], [6, 7, 0]]
    # Anschliessend werden die Ringe sowie die möglichen Mühlen durchgegangen und verglichen.
    for ring_nummer, ring_werte in enumerate(liste):
        for m in ring_muehlen:
            # Werden 3 Felder in einer der oben definierten Reihe alle mit dem gleichen Wert (nicht 0) gefunden,
            # so wird eine neue Liste mit allen Feldern sowie dem Spieler erstellt und bei den Ergebnissen angefügt.
            if ring_werte[m[0]] == ring_werte[m[1]] == ring_werte[m[2]] and not ring_werte[m[0]] == 0:
                neue_muehlen.append([[ring_nummer, m[0]], [ring_nummer, m[1]], [ring_nummer, m[2]], ring_werte[m[0]]])

    # Nach den Ringen folgen die senkrechten, zur mitte zeigende Mühlen.
    # Diese können nur bei den ungeraden stellen auf dem Ring gebildet werden.
    for w in [1, 3, 5, 7]:
        if liste[0][w] == liste[1][w] == liste[2][w] and not liste[0][w] == 0:
            neue_muehlen.append([[0, w], [1, w], [2, w], liste[0][w]])

    # Zum Schluss gilt es herauszufinden, ob die gefundenen Mühlen neu sind oder nicht.
    # Dabei interessiert insbesondere deren Anzahl.
    anz_neue_m = 0
    for r, s in enumerate(neue_muehlen):
        if s not in alte_muehlen:
            anz_neue_m += 1
    # Neben der Anzahl neuer Mühlen ist auch die Liste aller nun vorhandener Mühlen interessant.
    return neue_muehlen, anz_neue_m


def stein_entfernen_k(liste, muehlen, p, feld, ausgabe = False):
    # Überprüfung, ob auf dem Feld ein gegnerischer Stein liegt und dieser nicht in einer Mühle ist
    if ist_feld_frei(liste, feld) or liste[feld[0]][feld[1]] == p:
        if ausgabe:
            print("Da gibts nichts zum Klauen")
        return False
    else:
        for muehle in muehlen:
            if feld in muehle:
                if ausgabe:
                    print("Der Stein befindet sich in einer Mühle. Nimm bitte einen anderen")
                return False
        return True


def fahren_optionen(liste, feld, zielwert = 0):
    # Wie viele freie Optionen zum Fahren gibt es bei einem Feld?
    # Wichtig für die erkennung des Einklemmens sowie die Algorithmen
    optionen = 0
    # Der Ring soll ein Ring ohne unterbrechungen sein, Ecken werden einfach übersetzt
    ring_schliessen = {-1: 7, 0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 0}

    if liste[feld[0]][ring_schliessen[feld[1]+1]] ==  zielwert:
        # Zuerst auf dem Ring
        optionen += 1

    if liste[feld[0]][ring_schliessen[feld[1]-1]] ==  zielwert:
        # in beide Richtungen
        optionen += 1

    if feld[0] % 2 == 0 and feld[1] % 2 == 1 and liste[1][feld[1]] ==  zielwert:
        # Von den Seitenmitten (innen/ aussen) auf die Mitte
        optionen += 1

    if feld[0] == 1 and feld[1] % 2 == 1:
        # Von den Schlüsselpositionen nach innen / aussen
        for i in [0, 2]:
            if liste[i][feld[1]] ==  zielwert:
                optionen += 1
    return optionen



def einklemmen_k(liste, p):
    # Sämtliche Positionen werden durchgegeangen.
    for ring_nr, ring_werte in enumerate(liste):
        for pos_nr, pos_wert in enumerate(ring_werte):
            # Falls es sich um einen Stein des Spielers handelt, ...
            # ... und dieser sich bewegen kann, wird die Suche beendet
            if pos_wert == p and fahren_optionen(liste, [ring_nr, pos_nr]) > 0:
                return True
    # Falls jedoch keiner der Steine sich bewegen kann, so hat der Spieler verloren
    return False



def gewinner_erkennen(liste, gewinner, zu_frueh):
    # Schaut ob jemand weniger als 3 steine besitzt
    # Sendet anschliessend entweder False oder den Gewinner zurück
    verlierer = gewinner + 1
    if verlierer == 3:
        verlierer = 1
    n = 0
    for ring in liste:
        n += ring.count(verlierer)
    if n < 3 and zu_frueh > 16:
        print(f"Spieler {gewinner} hat gewonnen!")
        return gewinner
    else:
        return False



def muehle_vorschau(liste, spieler = 1, feld_3 = 0):
    potenzielle_m = []
    #2 Steine in einer ringförmiger mühle erkennen
    for ring_nummer, ring_werte in enumerate(liste):
        for m in [[0, 1, 2], [2, 3, 4], [4, 5, 6], [6, 7, 0]]:
            #für die jeweiligen 3 Lücken
            for w in [[0, 1, 2], [1, 2, 0], [2, 0, 1]]:
                if ring_werte[m[w[0]]] == ring_werte[m[w[1]]] == spieler and ring_werte[m[w[2]]] == feld_3:
                    potenzielle_m.append([ring_nummer, m[w[2]]])

    for m in [1, 3, 5, 7]:
        for w in [[0, 1, 2], [1, 2, 0], [2, 0, 1]]:
            if liste[w[0]][m] == liste[w[1]][m] == spieler and liste[w[2]][m] == feld_3:
                potenzielle_m.append([w[2], m])
    print(potenzielle_m)
    return potenzielle_m



def potenzielle_doppel_m(liste, spieler):
    pot_doppel_m = []
    for ring_nummer, ring_werte in enumerate(liste):
        for pos_nummer, pos_wert in enumerate(ring_werte):
            if pos_wert == spieler:
                #Vorzeitige Erkennung, ob sich 2 ring_m über eine ecke bilden können
                gefahr = False
                for radius in [3 - pos_nummer % 2, 4 - pos_nummer % 2]:
                    suchfeld = pos_nummer + radius
                    print(suchfeld)
                    if suchfeld >= 8:
                        suchfeld -= 8
                    if ring_werte[suchfeld] == spieler:
                        gefahr = True
                if gefahr:
                    for feld in range(4 - pos_nummer % 2):
                        suchfeld = pos_nummer + feld
                        if suchfeld >= 8:
                            suchfeld -= 8
                        if ring_werte[suchfeld] not in [0, spieler]:
                            gefahr = False
                if gefahr and pos_nummer <= 5:
                    pot_doppel_m.append([ring_nummer, pos_nummer + (2 - pos_nummer % 2)])
                if gefahr and pos_nummer in [6, 7]:
                    pot_doppel_m.append([ring_nummer, 0])


                """
                #kombination von senkrechten und ring m vorzeitig entdecken
                if pos_nummer in [1, 3, 5, 7]:
                    gefahr = False
                    for ring in range(3):
                        if liste[ring][pos_nummer - 1] == spieler:
                            gefahr = True

                   """
    return pot_doppel_m
