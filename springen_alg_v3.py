from ueberpruefungen import *
import random as rd

def springen_fe(liste, gef_felder, eigene_steine):
    for stein in eigene_steine:
        for feld in gef_felder:
            if stein == feld:
                eigene_steine.remove(stein)
                gef_felder.remove(feld)

    # Da die Listen nun Leer sein könnten, also alles abgedeckt,
    # müssen Werte hinzugefügt werden, da ein Zug obligatorisch ist.
    eigene_steine.append([rd.randint(0, 2), rd.randint(0, 7)])
    gef_felder.append([rd.randint(0, 2), rd.randint(0, 7)])
    return eigene_steine[0], gef_felder[0]


def springen_f2(liste, gef_felder, eigene_steine):
    zielfeld = muehle_vorschau(liste, 2)[-1]
    # Bei mehreren einträgen (2 Optionen über Ecke/ Kreuz)
    for feld in eigene_steine:
        # Mit der 2 an dritter Stelle wird die Anzahl benachbarter, eigener Steine zurückgegeben
        # Liegt einer der Steine abseits so wird er genutzt
        if fahren_optionen(liste, feld, 2) == 0:
            return feld, zielfeld
        elif fahren_optionen(liste, feld, 2) == 1 and not feld == zielfeld:
            return feld, zielfeld


def  springen_fm(liste, gef_felder, eigene_steine):
    # Die Mühle muss geöffnet werden, damit sie erneut geschlossen werden kann.
    # Falls dabei eine gefährliche Position existiert, so soll sie wieder eingenommen werden.
    if len(gef_felder) == 1:
        return eigene_steine[-1], gef_felder[-1]
    else:
        # Sonst auf eine zufällige Position
        return eigene_steine[-1], [rd.randint(0, 2), rd.randint(0, 7)]


def springen_f0(liste, gef_felder, eigene_steine):
    # Um auf dem Ring unabhängig vom Startpunkt 3 felder nach vorne gehen zu können,
    # wird der Neubeginn der Nummerierung durch dieses Dictionary überbrückt.
    ring_schliessen = {-3: 5, -2: 6, -1: 7, 0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 0, 9: 1, 10: 2}

    ziele = []
    for ring_nr, ring_werte in enumerate(liste.copy()):
        for pos_nr, pos_wert in enumerate(ring_werte):
            for i in range(4):
                if not fahren_optionen(liste, [ring_nr, ring_schliessen[pos_nr + i]], 1) == 0:
                    break
                elif i == 2 and pos_wert == 2:
                    for stein in eigene_steine:
                        if not stein == [ring_nr, ring_schliessen[pos_nr + 1]]:
                            return stein, [ring_nr, ring_schliessen[pos_nr + 1]]
                elif i == 3:
                    ziele.append([ring_nr, pos_nr])

    return eigene_steine[-1], ziele[-1]




def springen_f1(liste, gef_felder, eigene_steine):
    # Falls es eine Position zu blockieren gibt.
    # Wird diese Position schon blockiert?
    freie_steine = []
    for stein in eigene_steine:
        if stein not in gef_felder:
            freie_steine.append(stein)
    # Falls ja: Wie mit 3 freien, falls nein: den Stein blockieren
    if len(freie_steine) == 2:
        return springen_f0(liste, gef_felder, freie_steine)
    else:
        return freie_steine[-1], gef_felder[-1]






def springen_alg(liste):
    print("Springen-Algorithmus")
    # In dieser Phase des Spiels darf der Computer auf keinen Fall einen weiteren Stein verlieren.
    # Das Verhindern von Mühlen hat oberste Priorität
    # Welche Positionen gilt es unbedingt zu besetzten?
    zu_blockende_m = muehle_vorschau(liste, 1, 2)

    # Nun gilt es, die wirtlich gefährlichen Positionen auszumachen.
    # Das sind diejenigen, wo ein dritter Stein zur schliessung bereitsteht.
    gef_felder = []
    for feld in zu_blockende_m:
        if fahren_optionen(liste, feld, 1) > 0:
            gef_felder.append(feld)
    print(gef_felder)

    # Die Strategie hängt stark von der Anzahl der dafür verwendeten Steine ab.
    anz_notwendige_steine = len(gef_felder)

    # Da lediglich 3 Steine zur Verfügung stehen, macht es Sinn, deren Positionen zu speichern
    eigene_steine = []
    for ring_nr, ring_wert in enumerate(liste.copy()):
        for pos_nr, pos_wert in enumerate(ring_wert):
            if pos_wert == 2:
                # Zusätzlich ist wichtig, ob sie gerade eine Muehle blockieren
                eigene_steine.append([ring_nr, pos_nr])

    # Wenn 2 oder mehr Steine fürs blockieren benutzt werden müssen:
    if anz_notwendige_steine > 1:
        return springen_fe(liste, gef_felder, eigene_steine)

    # Sonst nur noch 1 oder 0
    # Mit etwas Glück oder guter Vorarbeit liegen bereits 2 Steine in einer Reihe:
    elif muehle_vorschau(liste, 2):
        return springen_f2(liste, gef_felder, eigene_steine)

    # Liegen die 3 Steine in einer Mühle?
    # Diese Funktion kann durch die 2 an 3. Stelle auch zur Mühle Überprüfung genutzt werden,
    # jedoch gibt sie nur eines der Felder zurück, für hier reicht dies jedoch.
    elif muehle_vorschau(liste, 2, 2):
        return springen_fm(liste, gef_felder, eigene_steine)

    # Vorarbeit mit 2 Steinen leisten
    elif anz_notwendige_steine == 1:
        return springen_f1(liste, gef_felder, eigene_steine)

    elif anz_notwendige_steine == 0:
        return springen_f0(liste, gef_felder, eigene_steine)





