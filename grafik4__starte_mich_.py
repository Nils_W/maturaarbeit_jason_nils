# Alles wichtige wird Importiert
import math
import turtle as t
import random as rd
import tkinter as tk
import time as time
import spiel_6 as spiel


def get_screen_size():
    # gibt die Grösse des Bildschirmes wieder, inklusive Skalierung von Windows
    root = tk.Tk()
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    root.destroy()
    print("Bildschirmgrösse:", screen_width, screen_height)
    return [screen_width, screen_height]


# Definiert die Positionen der Steine
def steinpositionen():
    x = t.size / 2
    y = t.size / 2
    dx = x / 3
    t.pos = []
    for i in range(3):
        t.pos.append([[-x, y], [0, y], [x, y], [x, 0], [x, -y], [0, -y], [-x, -y], [-x, 0]])
        x -= dx
        y -= dx


# Turtle verstecken und maximale Geschwindigkeit einstellen
def turtle_vorbereiten():
    global window_size
    t.colormode = 255
    t.ht()
    t.speed(0)
    t.tracer(False)


    t.screen = t.getscreen()
    t.screen.onclick(clicked)
    t.mouse = [0, 0]
    t.screensize(int(window_size[0] / 2), int(window_size[1] / 2))
    t.size = int(window_size[0] / 4)
    t.highlight = [-1, -1]
    steinpositionen()

    ws = t.screen.getcanvas()
    ws.bind("<Motion>", mousemoved)
    feld_zeichnen()



# Das Spielfeld wird gezeichnet
def feld_zeichnen():
    # Stiftdicke
    t.pensize(3)
    # Stiftfarbe
    t.pencolor(0, 0, 0)
    t.clear()

    # 3 Quadrate zeichnen
    for i in range(3):
        # Quadratliniuen verbinden
        for j in range(8):
            t.pu()
            t.goto(t.pos[i][j % 8])
            t.pd()
            t.goto(t.pos[i][(j + 1) % 8])

    # 4 Verbindungen zeichnen
    for i in range(4):
        t.pu()
        t.goto(t.pos[0][2 * i + 1])
        t.pd()
        t.goto(t.pos[2][2 * i + 1])


def ende(gewinner):
    t.reset()
    t.bgcolor("darkgrey")

    if gewinner == 1:
        t.color("blue")
    else:
        t.color("red")

    text = f"Glückwunsch Spieler {gewinner}, du hast gewonnen!"
    t.write(text, move=False, align="center", font = ("Arial", 24, "bold"))
    t.exitonclick()

def falsche_eingabe():
    t.bgcolor("red")
    time.sleep(0.5)
    t.bgcolor("white")


def getminimum(x, y):
    global z_liste, klicken_auf
    # Berechnen, zu welchem Punkt die Maus am nächsten ist
    dist = []
    # Liste der quadrierten Distanzen zur Maus berechnen
    # Sofern die Felder den Wert s besitzen
    for i in range(3):
        for j in range(8):
            if z_liste[i][j] == klicken_auf:
                dist.append((x - t.pos[i][j][0]) ** 2 + (y - t.pos[i][j][1]) ** 2)
            else:
                dist.append(9999999999999999999)

    # Das Minimum der Distanzen bestimmen
    minimum = min(dist)
    # Elementnummer des Minimums bestimmen
    n = dist.index(minimum)
    # Punkt zurückgeben
    return [n // 8, n % 8]


# Wenn die linke Maustaste gedrückt wird
def clicked(x, y):
    global  z_liste, klicken_auf
    click_punkt = getminimum(x, y)
    print("Clicked at:", x, y, " Entspricht:", t.pos[click_punkt[0]][click_punkt[1]], " index:", click_punkt)

    # Keinen weiteren input zulassen vor dem Abwickeln des zuges
    klicken_auf = 7

    # Die Eingabe soll nun genutzt werden, um einen Zug auszuführen
    z_liste, klicken_auf, korrekt, gewinner = spiel.zug(click_punkt)

    if gewinner:
        # Hat jemand gewonnen?
        ende(gewinner)

    if not korrekt:
        # Feedback bei fehlerhafter Eingabe.
        falsche_eingabe()


# Wenn die Maus bewegt wird
def mousemoved(event):
    global window_size, klicken_auf
    # Mauskoordinaten in Fensterkorrdinaten umrechnen
    t.mouse = [event.x - (window_size[0] / 4.1), (window_size[1] / 2.7) - event.y]

    x = t.mouse[0]
    y = t.mouse[1]

    # Den minimalen Punkt merken
    pos = getminimum(x, y)
    t.highlight = t.pos[pos[0]][pos[1]]


# Punkte einzeichnen
def punkte_zeichnen():
    global z_liste, klicken_auf
    # Farben vorbereiten
    p1 = "darkblue"  # Spieler 1 Farbe
    p2 = "darkred"  # Spieler 2 Farbe
    pn = "black"  # neutrale Farbe
    ph = "darkgreen"
    p_to_c = {0: pn, 1: p1, 2: p2, 3: ph}  # farben den spieler zuordnen

    for i in range(3):
        for j in range(8):
            t.pu()
            t.goto(t.pos[i][j])
            t.pd()

            t.pencolor(p_to_c[z_liste[i][j]])
            t.dot(32)

            if t.highlight == t.pos[i][j]:
                t.pencolor("yellow")
                t.dot(16)

    t.screen.update()


z_liste = spiel.daten_vorbereiten()

klicken_auf = 0
window_size = get_screen_size()
turtle_vorbereiten()
t.screen.update()



markieren = False
while True:
    punkte_zeichnen()





