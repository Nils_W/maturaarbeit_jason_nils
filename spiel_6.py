# Das Herzstück des Programms. Nach jeder eingabe wird die (unterste) Funktion zug(eingabe) aufgerufen.
# Diese holt zuerst sämtliche wichtigen Daten aus dem Speicher.
# Danach wird die Situation analysiert und die Eingabe verarbeitet.
# Nutzt neben den anderen im Dokument enthaltenen Funktionen:
# - Die jeweiligen Hauptfunktionen der drei Algorithmen.
# - Weitere Funktionen aus dem Dokument ueberpruefungen.


# Imports
import json
import random as rd
import ueberpruefungen as up
import setzten_alg_v5 as set_alg
import fahren_alg_v5 as fah_alg
import springen_alg_v3 as spr_alg



def laden(element = "liste", file = "daten.json"):
    # Die Daten aus dem abgespeicherten Dokument laden.
    # Daten sind dort in Form eines Dictionaries gespeichert.
    # Schlüssel wird übergeben, wert wird zurückgegeben.
    with open(file, "r") as f:
        ausgabe = json.load(f)[element]
        return ausgabe


def speichern(key, value, file = "daten.json"):
    # Die Daten in ein Dokument abspeichern.
    # Daten werden dort in Form eines Dictionaries gespeichert.
    # Schlüssel und Wert werden übergeben.
    # Damit die restlichen Daten im Dokument nicht verloren gehen, werden sie zuerst geladen,
    # ehe sie wieder gespeichert werden.
    with open(file, "r") as f:
        daten = json.load(f)
    with open(file, "w") as f:
        daten[key] = value
        json.dump(daten, f)


def listenanederung(liste, feld, p):
    # Änderungen an der Liste werden durch diese Funktion vorgenommen.
    # Die Modifikation wird gespeichert sowie auch zurückgesendet.
    liste[feld[0]][feld[1]] = p
    #abspeichern
    speichern("liste", liste)
    return liste



def setzen(liste, p, feld):
    # einen Stein setzen, p = spieler
    print(f"setzten bei: {feld}")
    # Falls das Feld frei ist:
    # Änderung an den Daten, gültiger Zug
    if up.ist_feld_frei(liste, feld):
        listenanederung(liste, feld, p)
        return liste, True
    # Sonst: ungültiger Zug
    else:
        return liste, False


def fahren_zug(liste, p, startfeld, zielfeld):
    # Fahren von Startfeld nach Zielfeld, p = spieler
    print(f"Fahren von: {startfeld} nach {zielfeld}")
    # Wenn der Zug gültig ist, werden die listen modifiziert und gültiger zug.
    if up.fahren_k(liste, startfeld, zielfeld):
        liste = listenanederung(liste, startfeld, 0)
        liste = listenanederung(liste, zielfeld, p)
        return liste, True
    else:
        # Ungültiger Zug
        print("Dieser Zug ist leider nicht möglich, bitte erneut versuchen!")
        return liste, False


def springen(liste, p, startfeld, zielfeld):
    # Springen von Startfeld nach Zielfeld, p = spieler
    print(f"Springen von: {startfeld} nach {zielfeld}")
    # Falls das Feld frei ist dann ist es ein gültiger zug, sonst nicht.
    if up.ist_feld_frei(liste, zielfeld):
        liste = listenanederung(liste, startfeld, 0)
        liste = listenanederung(liste, zielfeld, p)
        return liste, True

    else:
        print("Dieses Feld ist leider schon belegt, versuche es erneut")
        return liste, False


def stein_entfernen_zug(liste, muehlen, anz_z_kl_st, p, eingabe):
    # Wenn ein Spieler (p) einen Stein entfernen darf/muss.
    # Es kann nicht jeder Stein entfernt werden, er muss dem Gegner gehören und darf nicht in einer Mühle stecken.
    # Entweder gut: Stein aus Liste entfernen, gültiger Zug
    # Oder es ist ein ungültiger Zug: erneut versuchen
    if up.stein_entfernen_k(liste, muehlen, p, eingabe, True):
        liste = listenanederung(liste, eingabe, 0)
        speichern("anz_z_kl_st", anz_z_kl_st - 1)
        return liste, anz_z_kl_st - 1, True

    else:
        return liste, anz_z_kl_st, False


"""
Wird nicht gebraucht, da sonst zu viel Feedback → unübersichtlich und verwirrend

def markieren_alle(liste, was = 0):
    #markiert alle Steine mit einem bestimmten Wert
    #durch ersetzten der Werte durch 3
    m_liste = []
    for ring in liste:
        m_liste.append([])
        for wert in ring:
            if wert == was:
                m_liste[-1].append(3)
            else:
                m_liste[-1].append(wert)

    return m_liste
"""


def daten_vorbereiten():
    # Wird zu Beginn aufgerufen, speichert für alle Variablen und listen die Startwerte ab.

    # Um das Dokument zu erstellen, bei erster ausführung an neuem ort.
    with open("daten.json", "w") as f:
        daten = {"lol": []}
        json.dump(daten, f)

    # Liste für alle Positionen vorbereiten.
    # Wird in Kapitel 2.2 genauer erklärt.
    ring = []
    for i in range(8):
        ring.append(0)

    liste = [ring.copy(), ring.copy(), ring.copy()]
    speichern("liste", liste)

    # Spieler 1 beginnt.
    speichern("p", 1)
    # Es wurden bisher 0 Züge gespielt.
    speichern("zug_nr", 0)
    # Es gibt auch noch keine Mühlen und folglich dürfen 0 Steine entfernt werden.
    speichern("muehlen", [])
    speichern("anz_z_kl_st", 0)
    # Erst fürs Fahren wichtig, da dort 2 inputs gebraucht werden → Der erste wird gespeichert.
    speichern("offene_bewegung", None)

    # Die schlussendlich herausgegebene Liste weicht teilweise von der eigentlichen ab → Markierungen
    speichern("mark_liste", liste)

    # Alle Daten wurden erstellt, die leere Liste wird nun gezeichnet
    print(liste, "Daten vorbereitet")
    return liste


def spieler_zug(liste, muehlen, anz_z_kl_st, zug_nr, offene_bewegung, eingabe, p = 1):
    # Falls der Spieler an der Reihe ist
    gespielt = False
    anzahl_steine = up.anz_steine(liste)[p - 1]

    print("Spieler an der Reihe")
    print(f"Startfeld: {offene_bewegung}, Zielfeld: {eingabe}")

    # Darf er einen Stein entfernen?
    if  anz_z_kl_st:
        print("   → Stein entfernen")
        liste, anz_z_kl_st, gespielt = stein_entfernen_zug(liste, muehlen, anz_z_kl_st, p, eingabe)

    # Darf er noch setzten?
    elif zug_nr < 18:
        print("   → setzten")
        liste, gespielt = setzen(liste, p, eingabe)

    # Ist noch eine vorherige Eingabe gespeichert, sowie genügend steine fürs Fahren?
    elif offene_bewegung and anzahl_steine > 3:
        print("   → fahren")
        # Falls das Startfeld erneut gewäht wurde, so wird die Eingabe rückgängig gemacht.
        if eingabe == offene_bewegung:
            offene_bewegung = False
            speichern("offene_bewegung", offene_bewegung)
        else:
            liste, gespielt = fahren_zug(liste, p, offene_bewegung, eingabe)

    # Ist noch eine vorherige Eingabe gespeichert, sowie nur noch 3 Steine?
    elif offene_bewegung and anzahl_steine == 3:
        print("   → springen")
        liste, gespielt = springen(liste, p, offene_bewegung, eingabe)

    # Gehört das Feld dem Spieler?
    elif liste[eingabe[0]][eingabe[1]] == p:
        print("   → feld_ausgewählt")
        offene_bewegung = eingabe
        speichern("offene_bewegung",eingabe)

    else:
        print("fehler", zug_nr)

    return liste, offene_bewegung, gespielt


def alg_zug(liste, muehlen, anz_z_kl_st, zug_nr, p = 2):
    # Falls der Spieler an der Reihe ist
    # Analog wie beim Spieler, jedoch wird hier der input nicht gegeben, er muss vom
    # jeweiligen Algorithmus gefunden werden.
    print("Algorithmus an der Reihe")
    gespielt = False
    anzahl_steine = up.anz_steine(liste)[2 - 1]


    if anz_z_kl_st:
        # Gibt es noch Steine zu entfernen:
        print("   → Stein entfernen")
        wo = [rd.randint(0, 2), rd.randint(0, 7)]
        liste, anz_z_kl_st, gespielt = stein_entfernen_zug(liste, muehlen, anz_z_kl_st, 2, wo)

    elif zug_nr < 18:
        print("   → setzten")
        wo = set_alg.setzten_alg(liste)
        liste, gespielt = setzen(liste, 2, wo)

    elif  anzahl_steine > 3:
        print("   → fahren")
        start_f, ziel_f = fah_alg.fahren_alg(liste)
        liste, gespielt = fahren_zug(liste, 2, start_f, ziel_f)

    elif anzahl_steine == 3:
        print("   → springen")
        start_f, ziel_f = spr_alg.springen_alg(liste)
        liste, gespielt = springen(liste, p, start_f, ziel_f)

    return liste, gespielt


def markieren(liste, muehlen, anz_z_kl_st, offene_bewegung, zug_nr, p = 1):
    print("Markieren:")
    anzahl_steine = up.anz_steine(liste)[p-1]
    mark_liste = liste

    if anz_z_kl_st > 0:
        print("   → Entfernbare Steine")
        was_markieren = 3
        mark_liste = []
        for ring_nr, ring_val in enumerate(liste):
            mark_liste.append([])
            for pos, wert in enumerate(ring_val):
                if up.stein_entfernen_k(liste, muehlen,  p, [ring_nr, pos]):
                    mark_liste[-1].append(3)
                else:
                    mark_liste[-1].append(wert)
        print(mark_liste)


    elif zug_nr > 17 and not offene_bewegung:
        print("   → Eigene Steine")
        was_markieren = p
        """
        for ring_nr, ring_val in enumerate(liste):
            for pos, wert in enumerate(ring_val):
                if wert == p and fahren_optionen(liste, [ring_nr, pos]) > 0:
                    mark_liste[ring_nr][pos] = 3 """
        print(mark_liste)

    elif offene_bewegung and anzahl_steine > 3:
        was_markieren = 3
        print("   → Optionen zum Fahren")
        mark_liste = []
        for ring_nr, ring_val in enumerate(liste):
            mark_liste.append([])
            for pos, wert in enumerate(ring_val):
                if up.fahren_k(liste, offene_bewegung, [ring_nr, pos]) or offene_bewegung == [ring_nr, pos]:
                    mark_liste[-1].append(3)
                else:
                    mark_liste[-1].append(wert)

        print(mark_liste)

    else:
        was_markieren = 0
        print("   → Keine Markierungen notwendig.")

    return mark_liste, was_markieren


def zug(eingabe, pvp = False):
    # Der Kern des ganzen Programmes
    # Wird nach einer Eingabe aufgerufen

    print(f"Neue Eingabe: {eingabe}")

    # Zuerst alle wichtigen Daten aus dem Speicher holen
    # Erklärungen zu den Variablen finden sich bei der Funktion daten_vorbereiten
    p = laden("p")
    liste = laden("liste")
    muehlen = laden("muehlen")
    zug_nr = laden("zug_nr")

    anz_z_kl_st = laden("anz_z_kl_st")
    offene_bewegung = laden("offene_bewegung")



    # Der Spielzug dauert so lange, bis der Spieler wieder an der Reihe ist.
    # Denn der Algorithmus gibt seine Antwort direkt, er ruft die Funktion nicht erneut auf.
    spielzug = True

    while spielzug:
        # Die Variable gespielt steht für einen absolvierten Zug
        gespielt = False
        print("Neuer Durchgang:")
        print(liste)

        if p == 1 or pvp:
            # Handelt es sich um einen menschlichen Spieler?
            liste, offene_bewegung, gespielt = spieler_zug(liste, muehlen, anz_z_kl_st, zug_nr, offene_bewegung, eingabe, p)

        elif p == 2:
            # Der Computer ist an der Reihe
            liste, gespielt = alg_zug(liste, muehlen, anz_z_kl_st, zug_nr, offene_bewegung)

        # Hat der Spieler noch genügend steine?
        if up.gewinner_erkennen(liste, p, zug_nr):
            print("zu wenig Steine", p)
            return liste, 0, gespielt, p


        # Die Eingabe wurde verarbeitet und sofern sie gültig war, gab es Änderungen
        if gespielt:
            print(" → gültiger Zug")
            print(liste)

            offene_bewegung = False
            speichern("offene_bewegung", False)

            muehlen, anz_z_kl_st = up.muehle_ueberpruefung(liste, muehlen)
            speichern("muehlen", muehlen)
            speichern("anz_z_kl_st", anz_z_kl_st)

            # Falls nach einem erfolgreichen Zug keine Mühlen entstehen...
            # - so wird der Zug beendet und der nächste Folgt ⇾ zug_nr erhöhen
            # - wird der Spieler gewechselt.
            if anz_z_kl_st == 0:
                print("Keine Mühle gefunden.")
                # Der Zug ist zu Ende. → Der nächste beginnt.
                zug_nr += 1
                speichern("zug_nr", zug_nr)
                # Dafür wechselt der Spieler
                p += 1
                if p == 3:
                    p = 1
                speichern("p", p)
                # Der Loop wird nur beendet, wenn auch ein menschlicher Spieler an der Reihe ist.

            else:
                print("Mühle gefunden.")


            if p == 1 or pvp:
                print(" → Der Spieler ist wieder an der Reihe.")
                # Der Spieler kann erst einen neuen Zug machen, wenn auch der Computer gespielt hat.
                spielzug = False

            # Ist der Spieler eingeklemmt?
            if zug_nr > 17 and not up.einklemmen_k(liste, p):
                print("eingeklemmt")
                return liste, 0, gespielt, p



            # Der Loop wird nur beendet, wenn auch ein menschlicher Spieler an der Reihe ist.
            # Entweder nach einem Gültigen zug oder auch einem ungültigen (gespielt = False).
        elif p == 1 or pvp:
            print("Ungültiger Zug! Bitte mach einen gültigen Zug!")
            # Der Spieler kann erst einen neuen Zug machen, wenn auch der Computer gespielt hat.
            spielzug = False
        else:
            print("Ungültiger Zug! → Der Computer muss nochmals versuchen!")

        """        
        print(anz_z_kl_st)
        print(zug_nr)

        print(f"liste {liste}")
        print(f"ma_li {mark_liste}")
        print(f"muehlen {muehlen}")
        """
    print(liste)
    # Hilfsmarkierungen für den Spieler werden gemacht
    zeichnungsliste, was_markieren = markieren(liste, muehlen, anz_z_kl_st, offene_bewegung, zug_nr, 1)
    # Und zu guter letzt auch wieder dem Spieler zurückgegeben
    return zeichnungsliste, was_markieren, True, False



