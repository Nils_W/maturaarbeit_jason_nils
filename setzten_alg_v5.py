# Sucht die beste Position zum Setzen aus.
# Die (unterste) Funktion setzten_alg(liste) bildet dabei den Kern.
# Basiert auf einem Punktesystem.
# Nutzt neben der Funktion abstand(liste, gewichte) auch Funktionen aus der Datei Überprüfungen.

from ueberpruefungen import *

"""
def erster_stein(liste, spieler = 1):
    #als Erstes brauchen wir die Position des Steines
    gegner_pos = []
    for i, j in enumerate(liste):
        if spieler in j:
            gegner_pos = [i, j.index(spieler)]

    #falls er in einer Ecke gesetzt hat, setzen wir auf die nächste Schlüsselposition
    if gegner_pos[1] in [0, 2, 4, 6]:
        ziel_pos = [1, gegner_pos[1]+1]

    #Selbes gilt für mittelpositionen im inneren und äusseren Ring
    elif gegner_pos[0] in [0, 2]:
        ziel_pos = [1, gegner_pos[1]]

    #Falls er auch auf SP spielt, wird die benachbarte genommen.
    else:
        ziel_pos = [1, (gegner_pos[1]+2) % 8]

    return ziel_pos

"""

def abstand(liste, gewichte):
    # Verteilt Punkte für den Abstand zu anderen Steinen:
    # Je näher am Gegner, desto besser.
    # Auf 2 Felder Entfernung.


    # Um auf dem Ring unabhängig vom Startpunkt nach vorne oder hinten gehen zu können,
    # wird der Neubeginn der Nummerierung durch dieses Dictionary überbrückt.
    # Der Ring muss geschlossen werden, um wege zu ermöglichen. 8 wird wieder 0 und -1 wird zu 7.
    # Folglich auch -2 = 6 und 9 = , usw. Der Rest bleibt gleich.
    ring_schliessen = {-2:6, -1:7, 0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:0, 9:1}

    # Die Liste der Gewichte wird durchgegangen.
    # Diese ist analog zur normalen Liste aufgebaut, lediglich andere Werte.
    for ring_nr, ring_g in enumerate(gewichte):
        for pos_nr, pos_g in enumerate(ring_g):
            # Angrenzendes Feld auf dem Ring besetzt
            if liste[ring_nr][ring_schliessen[pos_nr + 1]] == 1:
                ring_g[pos_nr] += 10
            # 2 Felder entfernt
            if liste[ring_nr][ring_schliessen[pos_nr + 2]] == 1:
                ring_g[pos_nr] += 4
            # In die andere Richtung
            if liste[ring_nr][ring_schliessen[pos_nr - 1]] == 1:
                ring_g[pos_nr] += 10
            if liste[ring_nr][ring_schliessen[pos_nr - 2]] == 1:
                ring_g[pos_nr] += 4

            pass
            # Bei den Ecken des äusseren und Inneren Ringes
            if ring_nr % 2 == 0 and pos_nr % 2 == 0:
                # Schlüsselposition in distanz 2
                if liste[1][ring_schliessen[pos_nr - 1]] == 1:
                    ring_g[pos_nr] += 2
                if liste[1][ring_schliessen[pos_nr + 1]] == 1:
                    ring_g[pos_nr] += 2

            # Seitenmitte von Ring 0 und 2:
            elif ring_nr % 2 == 0 and pos_nr % 2 == 1:
                # Angrenzende Schlüsselposition: Stein hinstellen, um sie später einzunehmen.
                if liste[1][pos_nr] == 1:
                    ring_g[pos_nr] += 8
                # Bei Ecke des mittleren Ringes
                if liste[1][ring_schliessen[pos_nr + 1]] == 1:
                    ring_g[pos_nr] += 2
                if liste[1][ring_schliessen[pos_nr - 1]] == 1:
                    ring_g[pos_nr] += 2


            # Bei Ecken des mittleren Ringes
            elif ring_nr == 1 and pos_nr % 2 == 0:
                # Seitenmitten neben angrenzenden Schlüsselpositionen
                # zuerst Aussen
                if liste[0][ring_schliessen[pos_nr + 1]] == 1:
                    ring_g[pos_nr] += 6
                if liste[0][ring_schliessen[pos_nr - 1]] == 1:
                    ring_g[pos_nr] += 6
                # Dann innen
                if liste[2][ring_schliessen[pos_nr + 1]] == 1:
                    ring_g[pos_nr] += 6
                if liste[2][ring_schliessen[pos_nr - 1]] == 1:
                    ring_g[pos_nr] += 6

            # Bei Schlüsselpositionen
            elif ring_nr == 1 and pos_nr % 2 == 1:
                for i in [0, 2]:
                    # Bei angrenzender Seitenmitte
                    if liste[i][pos_nr] == 1:
                        ring_g[pos_nr] += 12
                    # Bei Ecke des mittleren Ringes
                    if liste[i][ring_schliessen[pos_nr + 1]] == 1:
                        ring_g[pos_nr] += 8
                    if liste[i][ring_schliessen[pos_nr - 1]] == 1:
                        ring_g[pos_nr] += 8

            # Nicht mögliche Positionen können nicht belegt werden, egal wie günstig sie wären.
            if not liste[ring_nr][pos_nr] == 0:
                ring_g[pos_nr] = 0

    return gewichte




def setzten_alg(liste):
    print("Setzten-Algorithmus")
    #Eine Gewichts-Liste mit allen Positionen und deren Startwerte wird definiert.
    # Diese bildet den Kern des Algorithms.
    #Ecke Aussen/ innen: 10, Ecke mitte: 15, Seiten: 20, Schlüsselpositionen: 30
    ea = 8
    em = 12
    sm = 16
    sp = 24
    ring_02 = [ea, sm, ea,sm, ea, sm, ea, sm]
    ring_1 =  [em, sp, em, sp, em, sp, em, sp]
    gewichte = [ring_02.copy(), ring_1.copy(), ring_02.copy()]

    # Danach alle Felder durchgehen
    for ring_nr, ring_werte in enumerate(liste):
        for pos_nr, pos_wert in enumerate(ring_werte):
            if not pos_wert == 0:
                # Bereits belegte Felder können nicht erneut benutzt werden → auf 0 setzen
                gewichte[ring_nr][pos_nr] = 0
            else:
                # Die Bewegungsfreiheit der Felder ist wichtig:
                # Daher wird diese bewertet
                gewichte[ring_nr][pos_nr] += 12 * fahren_optionen(liste, [ring_nr, pos_nr])

    # Hat der Gegner eine Mühle vorbereitet?
    muehle_blockieren = muehle_vorschau(liste)
    for gef_feld in muehle_blockieren:
        gewichte[gef_feld[0]][gef_feld[1]] += 38

    # Habe ich selbst bereits 2 Steine in einer Reihe?
    eigene_m = muehle_vorschau(liste, 2)
    for schliess_feld in eigene_m:
        gewichte[schliess_feld[0]][schliess_feld[1]] += 28

    # Da die Bewegungsfreiheit sehr wichtig ist, geht es darum, die des Gegners so klein wie möglich zu halten.
    gewichte = abstand(liste, gewichte)

    print(gewichte)

    # Zu guter letzt muss noch der beste Spielzug ausgesucht werden.
    # → Derjenige mit der höchsten Gewichtung.
    # Höchster Wert und dessen position werden auf 0 gesetzt, danach wird die Gewichtsliste durchgegangen.
    # Falls ein Wert grösser ist, so wird er unter diesen Variablen gespeichert.
    h_wert = 0
    h_pos = [0, 0]
    for ring_nr, ring_g in enumerate(gewichte):
        for pos_nr, pos_g in enumerate(ring_g):
            if pos_g > h_wert:
                h_wert = pos_g
                h_pos = [ring_nr, pos_nr]
    # Die Position des besten wertes ist wichtig, um dann dort zu setzten.
    print(f"Antwort des Setzten-Algorithmus: {h_pos}")
    return h_pos



