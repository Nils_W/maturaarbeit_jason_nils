from ueberpruefungen import *
import json


def speichern(value):
    with open("fahren_temp.json", "w") as f:
        daten = {"urspruengliche_liste": value}
        json.dump(daten, f)


#Die Liste laden
def liste_laden():
    with open("fahren_temp.json", "r") as f:
        return json.load(f)["urspruengliche_liste"]


def schritt(urspruengliche_liste, p):
    laenge = 0
    # Der Ring soll ein Ring ohne unterbrechungen sein, Ecken werden einfach übersetzt
    ring_schliessen = {-1: 7, 0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 0}
    neue_listen = []
    speichern(urspruengliche_liste)

    for ring_nr, ring_wert in enumerate(urspruengliche_liste.copy()):
        for pos_nr, pos_wert in enumerate(ring_wert):
            # Falls es sich um einen Stein des Spielers handelt, ...
            # ... und dieser sich bewegen kann, wird die Suche beendet
            if pos_wert == p:
                if ring_wert[ring_schliessen[pos_nr - 1]] == 0:
                    # Zuerst auf dem Ring
                    temp_liste_a = liste_laden()
                    temp_liste_a[ring_nr][pos_nr] = 0
                    temp_liste_a[ring_nr][ring_schliessen[pos_nr - 1]] = p
                    neue_listen.append(temp_liste_a)

                if ring_wert[ring_schliessen[pos_nr + 1]] == 0:
                    # In beide Richtungen
                    temp_liste_b = liste_laden()
                    temp_liste_b[ring_nr][pos_nr] = 0
                    temp_liste_b[ring_nr][ring_schliessen[pos_nr + 1]] = p
                    neue_listen.append(temp_liste_b)

                if ring_nr % 2 == 0 and pos_nr % 2 == 1 and urspruengliche_liste.copy()[1][pos_nr] == 0:
                    # Von den Seitenmitten (innen/ aussen) auf die Mitte
                    temp_liste_c = liste_laden()
                    temp_liste_c[ring_nr][pos_nr] = 0
                    temp_liste_c[1][pos_nr] = p
                    neue_listen.append(temp_liste_c)

                if ring_wert == 1 and pos_wert % 2 == 1:
                    # Von den Schlüsselpositionen nach innen / aussen
                    for i in [0, 2]:
                        if liste[i][pos_nr] == 0:
                            temp_liste_d = liste_laden()
                            temp_liste_d[ring_nr][pos_nr] = 0
                            temp_liste_d[i][pos_nr] = p
                            neue_listen.append(temp_liste_d)

    laenge += len(neue_listen)
    return neue_listen


def auswerten(liste):
    zahl = 0
    for ring_nr, ring_wert in enumerate(liste):
        for pos_nr, pos_wert in enumerate(ring_wert):
            if pos_wert == 2:
                zahl += fahren_optionen(liste, [ring_nr, pos_nr])
            elif pos_wert == 1:
                zahl -= 2* fahren_optionen(liste, [ring_nr, pos_nr])

    muehlen, mich_brauchts_nicht = muehle_ueberpruefung(liste)
    for m in muehlen:
        if m[-1] == 2:
            zahl += 8
        else:
            zahl -= 8

    return zahl + 20


def zug_herausfinden(originale_liste, sieger_liste):
    startfeld, zielfeld = [], []
    for ring_nr, ring_wert in enumerate(originale_liste):
        for pos_nr, pos_wert in enumerate(ring_wert):
            if not sieger_liste[ring_nr][pos_nr] == pos_wert:
                if pos_wert == 0:
                    zielfeld = [ring_nr, pos_nr]
                else:
                    startfeld = [ring_nr, pos_nr]

    return startfeld, zielfeld



def fahren_alg(orriginale_l):
    print("Fahren-Algorithmus")
    level = 3

    beste_wertung = 0
    bester_zug = orriginale_l.copy()

    print(orriginale_l)
    for a in schritt(orriginale_l.copy(), 2):
        if level == 1 and auswerten(a) > beste_wertung:
            beste_wertung, bester_zug = auswerten(a), a

        elif level > 1:
            for b in schritt(a, 1):
                if level == 2 and auswerten(b) > beste_wertung:
                    beste_wertung, bester_zug = auswerten(b), a

                elif level > 2:
                    for c in schritt(b, 2):
                        if level == 3 and auswerten(c) > beste_wertung:
                            beste_wertung, bester_zug = auswerten(c), a

                        elif level > 3:
                            for d in schritt(c, 1):
                                if level == 4 and auswerten(d) > beste_wertung:
                                    beste_wertung, bester_zug = auswerten(d), a


                            """
                            if level > 3:
                                d_listen = []
                                for d in c:
                                    d_listen.append(schritt(d.copy(), 2))
                                    if level > 4:
                                        e_listen = []
                                        for e in c:
                                            e_listen.append(schritt(e].copy(), 2))

                                        d_listen.append(e_listen)
                                c_listen.append(d_listen)
           
                                """
    start_f, ziel_f = zug_herausfinden(orriginale_l, bester_zug)
    print(start_f, "gut?", ziel_f)
    return start_f, ziel_f




"""
ring0 = [1, 0, 0, 0, 1, 0, 0, 0]
ring1 = [1, 0, 0, 1, 0, 0, 0, 0]
ring2 = [2, 0, 0, 0, 2, 0, 2, 0]

print([ring0, ring1, ring2])
print(schritt([ring0, ring1, ring2], 2))

print(fahren_alg([ring0, ring1, ring2]))
"""